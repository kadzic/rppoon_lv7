﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp28
{
    class Program
    {
        static void Main(string[] args)
        {

            double[] array = new double[] { 1.5, 10.1, 12.0, 3.45, 6.12, 4.97, 37.43, 44.1, 2.2, 1.5, 3.9 };

            NumberSequence numberSequence = new NumberSequence(array);

            SearchElement findElement = new SearchElement();

            numberSequence.SetSearchStrategy(findElement);

            Console.WriteLine("Array elements: ");
            Console.WriteLine(numberSequence.ToString());

            Console.WriteLine("Write an element you want to search: ");
            double element = Convert.ToDouble(Console.ReadLine());

            findElement.Search(array, element);

            if(findElement.Search(array, element) == true)
            {
                Console.WriteLine("Element found. {0}", element);
            }
            else
            {
                Console.WriteLine("Element not found. {0}", element);
            }
        
        }
    }
}
