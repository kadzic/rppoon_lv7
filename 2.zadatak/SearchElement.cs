﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp28
{
    class SearchElement : SearchStrategy
    {
        public override bool Search(double[] array, double element)
        {
            bool elementFound = false;
            for(int i = 0; i < array.Length; i++)
            {
                if(element == array[i])
                {
                    elementFound = true;
                }
            }
            return elementFound;
        }
    }
}
