﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp28
{
    abstract class SearchStrategy
    {
        public abstract bool Search(double[] array, double element);
    }
}
