﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp28
{
    class Program
    {
        static void Main(string[] args)
        {

            BubbleSort bubbleSort = new BubbleSort();

            double[] array = new double[] { 1.5, 10.1, 12.0, 3.45, 6.12, 4.97, 37.43, 44.1, 2.2, 1.5, 3.9 };
            NumberSequence numberSequence = new NumberSequence(array);

            numberSequence.SetSortStrategy(bubbleSort);

            Console.WriteLine("Normal elements: ");
            Console.WriteLine(numberSequence.ToString());

            Console.WriteLine("Sorted elements: ");
            numberSequence.Sort();
            Console.WriteLine(numberSequence.ToString());
           

        }
    }
}
